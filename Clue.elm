module Clue exposing (nextClue, takeTwoSilentMoves, view)

import Html exposing (..)
import Model exposing (..)
import Styled exposing (..)
import Views.ActionButtons as ActionButtons


-- UPDATE


nextClue : Game -> Game
nextClue game =
    case List.head game.path.futureMoves of
        Just move ->
            if move.takeMoney then
                { game
                    | path = updatePath game.path move
                    , cash = game.cash - 5000
                    , crimesCommitted = game.crimesCommitted + 1
                }
            else
                { game | path = updatePath game.path move }

        Nothing ->
            -- This shouldn't happen
            -- (game should end with all arrested or no money)
            { game | state = PostGame }


updatePath : Path -> Move -> Path
updatePath path move =
    { path
        | futureMoves = List.drop 1 path.futureMoves
        , movesTaken = move :: path.movesTaken
    }


takeTwoSilentMoves : Game -> Game
takeTwoSilentMoves game =
    case List.take 2 game.path.futureMoves of
        move1 :: move2 :: rest ->
            let
                ( pathAfterMove1, moneyTaken1 ) =
                    takeSilentMove game.path move1

                ( pathAfterMove2, moneyTaken2 ) =
                    takeSilentMove pathAfterMove1 move2

                ( updatedCrimes, updatedCash ) =
                    if moneyTaken1 || moneyTaken2 then
                        ( game.crimesCommitted + 1, game.cash - 5000 )
                    else
                        ( game.crimesCommitted, game.cash )
            in
            nextClue
                { game
                    | path = pathAfterMove2
                    , cash = updatedCash
                    , state = InProgress ShowHint
                    , crimesCommitted = updatedCrimes
                }

        -- This shouldn't happen
        -- (game should end with all arrested or no money)
        _ ->
            { game | state = PostGame }


takeSilentMove : Path -> Move -> ( Path, Bool )
takeSilentMove path move =
    let
        silencedMove =
            { move | sound = Silent }

        updatedPath =
            { path
                | futureMoves = List.drop 1 path.futureMoves
                , movesTaken = silencedMove :: path.movesTaken
            }
    in
    ( updatedPath, silencedMove.takeMoney )



-- VIEW


clueText =
    styled div
        [ fontSize (px 30)
        , fontWeight (int 800)
        , marginBottom (px 30)
        ]


clueMeta =
    styled div
        [ marginTop (px 30)
        ]


view : Path -> Html Msg
view path =
    div []
        [ clue path
        , ActionButtons.view path
        ]


clue : Path -> Html Msg
clue path =
    let
        clueMetaInfo =
            if List.length path.movesTaken == 1 then
                buildingName path.startBuilding
            else
                "Clue #" ++ toString (List.length path.movesTaken)
    in
    div []
        [ clueMeta [] [ text <| clueMetaInfo ]
        , clueText [] [ text <| getSound <| List.head path.movesTaken ]
        ]


buildingName : Building -> String
buildingName building =
    case building of
        DRDOYLE ->
            "Dr Doyle's Electronics"

        SWINNERTONS ->
            "Swinnerton's"

        MUSEUM ->
            "Museum of Modern Antiquity"

        BANK ->
            "Trust US Bank"


getSound : Maybe Move -> String
getSound maybeMove =
    case maybeMove of
        Just move ->
            toString move.sound

        Nothing ->
            "..."
