module Paths.Path2 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 2
    , difficulty = Medium
    , startBuilding = BANK
    , futureMoves =
        [ { space = "466", sound = Footsteps, takeMoney = False }
        , { space = "467", sound = Crime, takeMoney = True }
        , { space = "468", sound = Door, takeMoney = False }
        , { space = "879", sound = Street, takeMoney = False }
        , { space = "699", sound = Subway, takeMoney = False }
        , { space = "679", sound = Street, takeMoney = False }
        , { space = "278", sound = Door, takeMoney = False }
        , { space = "277", sound = Crime, takeMoney = True }
        , { space = "276", sound = Footsteps, takeMoney = False }
        , { space = "265", sound = Crime, takeMoney = True }
        , { space = "255", sound = Door, takeMoney = False }
        , { space = "246", sound = Crime, takeMoney = True }
        , { space = "235", sound = Window, takeMoney = False }
        , { space = "224", sound = Footsteps, takeMoney = False }
        , { space = "214", sound = Door, takeMoney = False }
        , { space = "703", sound = Street, takeMoney = False }
        , { space = "701", sound = Street, takeMoney = False }
        , { space = "599", sound = Subway, takeMoney = False }
        , { space = "579", sound = Street, takeMoney = False }
        , { space = "559", sound = Street, takeMoney = False }
        , { space = "148", sound = Window, takeMoney = False }
        , { space = "147", sound = Footsteps, takeMoney = False }
        , { space = "146", sound = Crime, takeMoney = True }
        , { space = "136", sound = Door, takeMoney = False }
        , { space = "126", sound = Footsteps, takeMoney = False }
        , { space = "125", sound = Door, takeMoney = False }
        , { space = "144", sound = Crime, takeMoney = True }
        , { space = "124", sound = Door, takeMoney = False }
        , { space = "123", sound = Crime, takeMoney = True }
        , { space = "113", sound = Door, takeMoney = False }
        , { space = "413", sound = Door, takeMoney = False }
        , { space = "424", sound = Door, takeMoney = False }
        , { space = "425", sound = Crime, takeMoney = True }
        , { space = "426", sound = Door, takeMoney = False }
        , { space = "437", sound = Door, takeMoney = False }
        , { space = "446", sound = Footsteps, takeMoney = False }
        , { space = "456", sound = Door, takeMoney = False }
        , { space = "445", sound = Crime, takeMoney = True }
        , { space = "444", sound = Door, takeMoney = False }
        , { space = "443", sound = Footsteps, takeMoney = False }
        , { space = "432", sound = Footsteps, takeMoney = False }
        , { space = "442", sound = Footsteps, takeMoney = False }
        , { space = "441", sound = Door, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "352", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
