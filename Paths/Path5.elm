module Paths.Path5 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 5
    , difficulty = Medium
    , startBuilding = DRDOYLE
    , futureMoves =
        [ { space = "174", sound = Footsteps, takeMoney = False }
        , { space = "172", sound = Footsteps, takeMoney = False }
        , { space = "171", sound = Window, takeMoney = False }
        , { space = "271", sound = Window, takeMoney = False }
        , { space = "272", sound = Footsteps, takeMoney = False }
        , { space = "273", sound = Footsteps, takeMoney = False }
        , { space = "274", sound = Footsteps, takeMoney = False }
        , { space = "275", sound = Footsteps, takeMoney = False }
        , { space = "276", sound = Footsteps, takeMoney = False }
        , { space = "277", sound = Crime, takeMoney = True }
        , { space = "267", sound = Door, takeMoney = False }
        , { space = "247", sound = Crime, takeMoney = True }
        , { space = "237", sound = Door, takeMoney = False }
        , { space = "227", sound = Footsteps, takeMoney = False }
        , { space = "217", sound = Door, takeMoney = False }
        , { space = "317", sound = Door, takeMoney = False }
        , { space = "327", sound = Footsteps, takeMoney = False }
        , { space = "337", sound = Crime, takeMoney = True }
        , { space = "326", sound = Footsteps, takeMoney = False }
        , { space = "325", sound = Door, takeMoney = False }
        , { space = "324", sound = Footsteps, takeMoney = False }
        , { space = "333", sound = Footsteps, takeMoney = False }
        , { space = "344", sound = Door, takeMoney = False }
        , { space = "345", sound = Footsteps, takeMoney = False }
        , { space = "355", sound = Crime, takeMoney = True }
        , { space = "368", sound = Door, takeMoney = False }
        , { space = "376", sound = Crime, takeMoney = True }
        , { space = "375", sound = Footsteps, takeMoney = False }
        , { space = "374", sound = Footsteps, takeMoney = False }
        , { space = "373", sound = Door, takeMoney = False }
        , { space = "372", sound = Footsteps, takeMoney = False }
        , { space = "371", sound = Window, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "352", sound = Crime, takeMoney = True }
        , { space = "362", sound = Door, takeMoney = False }
        , { space = "371", sound = Window, takeMoney = False }
        , { space = "471", sound = Door, takeMoney = False }
        , { space = "472", sound = Footsteps, takeMoney = False }
        , { space = "473", sound = Crime, takeMoney = True }
        , { space = "464", sound = Door, takeMoney = False }
        , { space = "473", sound = Crime, takeMoney = True }
        , { space = "466", sound = Footsteps, takeMoney = False }
        , { space = "467", sound = Crime, takeMoney = True }
        , { space = "456", sound = Door, takeMoney = False }
        , { space = "445", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
