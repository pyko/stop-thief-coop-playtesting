module Paths.Path7 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 7
    , difficulty = Hard
    , startBuilding = MUSEUM
    , futureMoves =
        [ { space = "374", sound = Footsteps, takeMoney = False }
        , { space = "375", sound = Footsteps, takeMoney = False }
        , { space = "376", sound = Crime, takeMoney = True }
        , { space = "386", sound = Door, takeMoney = False }
        , { space = "797", sound = Street, takeMoney = False }
        , { space = "899", sound = Subway, takeMoney = False }
        , { space = "879", sound = Street, takeMoney = False }
        , { space = "468", sound = Door, takeMoney = False }
        , { space = "467", sound = Crime, takeMoney = True }
        , { space = "466", sound = Footsteps, takeMoney = False }
        , { space = "475", sound = Crime, takeMoney = True }
        , { space = "464", sound = Door, takeMoney = False }
        , { space = "473", sound = Crime, takeMoney = True }
        , { space = "463", sound = Footsteps, takeMoney = False }
        , { space = "443", sound = Footsteps, takeMoney = False }
        , { space = "444", sound = Door, takeMoney = False }
        , { space = "445", sound = Crime, takeMoney = True }
        , { space = "446", sound = Footsteps, takeMoney = False }
        , { space = "437", sound = Door, takeMoney = False }
        , { space = "426", sound = Door, takeMoney = False }
        , { space = "425", sound = Crime, takeMoney = True }
        , { space = "424", sound = Door, takeMoney = False }
        , { space = "413", sound = Door, takeMoney = False }
        , { space = "113", sound = Door, takeMoney = False }
        , { space = "123", sound = Crime, takeMoney = True }
        , { space = "124", sound = Door, takeMoney = False }
        , { space = "144", sound = Crime, takeMoney = True }
        , { space = "154", sound = Footsteps, takeMoney = False }
        , { space = "164", sound = Crime, takeMoney = True }
        , { space = "165", sound = Door, takeMoney = False }
        , { space = "166", sound = Footsteps, takeMoney = False }
        , { space = "167", sound = Footsteps, takeMoney = False }
        , { space = "157", sound = Door, takeMoney = False }
        , { space = "146", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
