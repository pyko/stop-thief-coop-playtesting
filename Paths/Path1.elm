module Paths.Path1 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 1
    , difficulty = Medium
    , startBuilding = DRDOYLE
    , futureMoves =
        [ { space = "174", sound = Footsteps, takeMoney = False }
        , { space = "164", sound = Crime, takeMoney = True }
        , { space = "165", sound = Door, takeMoney = False }
        , { space = "166", sound = Footsteps, takeMoney = False }
        , { space = "167", sound = Footsteps, takeMoney = False }
        , { space = "168", sound = Door, takeMoney = False }
        , { space = "559", sound = Street, takeMoney = False }
        , { space = "148", sound = Window, takeMoney = False }
        , { space = "157", sound = Door, takeMoney = False }
        , { space = "146", sound = Crime, takeMoney = True }
        , { space = "136", sound = Door, takeMoney = False }
        , { space = "127", sound = Footsteps, takeMoney = False }
        , { space = "117", sound = Door, takeMoney = False }
        , { space = "505", sound = Street, takeMoney = False }
        , { space = "503", sound = Street, takeMoney = False }
        , { space = "413", sound = Door, takeMoney = False }
        , { space = "423", sound = Footsteps, takeMoney = False }
        , { space = "432", sound = Footsteps, takeMoney = False }
        , { space = "443", sound = Footsteps, takeMoney = False }
        , { space = "444", sound = Door, takeMoney = False }
        , { space = "445", sound = Crime, takeMoney = True }
        , { space = "446", sound = Footsteps, takeMoney = False }
        , { space = "447", sound = Footsteps, takeMoney = False }
        , { space = "456", sound = Door, takeMoney = False }
        , { space = "467", sound = Crime, takeMoney = True }
        , { space = "477", sound = Door, takeMoney = False }
        , { space = "466", sound = Footsteps, takeMoney = False }
        , { space = "475", sound = Crime, takeMoney = True }
        , { space = "464", sound = Door, takeMoney = False }
        , { space = "473", sound = Crime, takeMoney = True }
        , { space = "472", sound = Footsteps, takeMoney = False }
        , { space = "471", sound = Door, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "352", sound = Crime, takeMoney = True }
        , { space = "362", sound = Door, takeMoney = False }
        , { space = "373", sound = Door, takeMoney = False }
        , { space = "384", sound = Window, takeMoney = False }
        , { space = "795", sound = Street, takeMoney = False }
        , { space = "386", sound = Door, takeMoney = False }
        , { space = "376", sound = Crime, takeMoney = True }
        , { space = "365", sound = Door, takeMoney = False }
        , { space = "355", sound = Crime, takeMoney = True }
        , { space = "344", sound = Door, takeMoney = False }
        , { space = "333", sound = Footsteps, takeMoney = False }
        , { space = "324", sound = Footsteps, takeMoney = False }
        , { space = "325", sound = Door, takeMoney = False }
        , { space = "326", sound = Footsteps, takeMoney = False }
        , { space = "337", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
