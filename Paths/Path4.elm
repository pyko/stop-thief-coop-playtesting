module Paths.Path4 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 4
    , difficulty = Easy
    , startBuilding = SWINNERTONS
    , futureMoves =
        [ { space = "276", sound = Footsteps, takeMoney = False }
        , { space = "275", sound = Footsteps, takeMoney = False }
        , { space = "265", sound = Crime, takeMoney = True }
        , { space = "276", sound = Footsteps, takeMoney = False }
        , { space = "277", sound = Crime, takeMoney = True }
        , { space = "267", sound = Door, takeMoney = False }
        , { space = "247", sound = Crime, takeMoney = True }
        , { space = "237", sound = Door, takeMoney = False }
        , { space = "228", sound = Window, takeMoney = False }
        , { space = "619", sound = Street, takeMoney = False }
        , { space = "709", sound = Street, takeMoney = False }
        , { space = "707", sound = Street, takeMoney = False }
        , { space = "317", sound = Door, takeMoney = False }
        , { space = "327", sound = Footsteps, takeMoney = False }
        , { space = "337", sound = Crime, takeMoney = True }
        , { space = "326", sound = Footsteps, takeMoney = False }
        , { space = "325", sound = Door, takeMoney = False }
        , { space = "324", sound = Footsteps, takeMoney = False }
        , { space = "314", sound = Door, takeMoney = False }
        , { space = "214", sound = Door, takeMoney = False }
        , { space = "223", sound = Footsteps, takeMoney = False }
        , { space = "233", sound = Footsteps, takeMoney = False }
        , { space = "242", sound = Crime, takeMoney = True }
        , { space = "231", sound = Window, takeMoney = False }
        , { space = "131", sound = Door, takeMoney = False }
        , { space = "132", sound = Door, takeMoney = False }
        , { space = "153", sound = Footsteps, takeMoney = False }
        , { space = "154", sound = Footsteps, takeMoney = False }
        , { space = "164", sound = Crime, takeMoney = True }
        , { space = "174", sound = Footsteps, takeMoney = False }
        , { space = "186", sound = Window, takeMoney = False }
        , { space = "597", sound = Street, takeMoney = False }
        , { space = "599", sound = Subway, takeMoney = False }
        , { space = "579", sound = Street, takeMoney = False }
        , { space = "559", sound = Street, takeMoney = False }
        , { space = "148", sound = Window, takeMoney = False }
        , { space = "157", sound = Door, takeMoney = False }
        , { space = "146", sound = Crime, takeMoney = True }
        , { space = "136", sound = Door, takeMoney = False }
        , { space = "127", sound = Footsteps, takeMoney = False }
        , { space = "117", sound = Door, takeMoney = False }
        , { space = "126", sound = Footsteps, takeMoney = False }
        , { space = "125", sound = Door, takeMoney = False }
        , { space = "144", sound = Crime, takeMoney = True }
        , { space = "124", sound = Door, takeMoney = False }
        , { space = "113", sound = Door, takeMoney = False }
        , { space = "413", sound = Door, takeMoney = False }
        , { space = "424", sound = Door, takeMoney = False }
        , { space = "425", sound = Crime, takeMoney = True }
        , { space = "426", sound = Door, takeMoney = False }
        , { space = "437", sound = Door, takeMoney = False }
        , { space = "446", sound = Footsteps, takeMoney = False }
        , { space = "445", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
