module Paths.Path8 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 8
    , difficulty = Easy
    , startBuilding = BANK
    , futureMoves =
        [ { space = "442", sound = Footsteps, takeMoney = False }
        , { space = "463", sound = Footsteps, takeMoney = False }
        , { space = "473", sound = Crime, takeMoney = True }
        , { space = "464", sound = Door, takeMoney = False }
        , { space = "475", sound = Crime, takeMoney = True }
        , { space = "456", sound = Door, takeMoney = False }
        , { space = "445", sound = Crime, takeMoney = True }
        , { space = "444", sound = Door, takeMoney = False }
        , { space = "443", sound = Footsteps, takeMoney = False }
        , { space = "432", sound = Footsteps, takeMoney = False }
        , { space = "423", sound = Footsteps, takeMoney = False }
        , { space = "424", sound = Door, takeMoney = False }
        , { space = "425", sound = Crime, takeMoney = True }
        , { space = "426", sound = Door, takeMoney = False }
        , { space = "437", sound = Door, takeMoney = False }
        , { space = "446", sound = Footsteps, takeMoney = False }
        , { space = "445", sound = Footsteps, takeMoney = False }
        , { space = "444", sound = Door, takeMoney = False }
        , { space = "432", sound = Footsteps, takeMoney = False }
        , { space = "442", sound = Footsteps, takeMoney = False }
        , { space = "441", sound = Door, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "352", sound = Crime, takeMoney = True }
        , { space = "362", sound = Door, takeMoney = False }
        , { space = "371", sound = Window, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "352", sound = Footsteps, takeMoney = False }
        , { space = "342", sound = Crime, takeMoney = True }
        , { space = "332", sound = Door, takeMoney = False }
        , { space = "333", sound = Footsteps, takeMoney = False }
        , { space = "344", sound = Door, takeMoney = False }
        , { space = "355", sound = Crime, takeMoney = True }
        , { space = "345", sound = Footsteps, takeMoney = False }
        , { space = "344", sound = Door, takeMoney = False }
        , { space = "333", sound = Footsteps, takeMoney = False }
        , { space = "324", sound = Footsteps, takeMoney = False }
        , { space = "314", sound = Door, takeMoney = False }
        , { space = "705", sound = Street, takeMoney = False }
        , { space = "317", sound = Door, takeMoney = False }
        , { space = "326", sound = Footsteps, takeMoney = False }
        , { space = "337", sound = Crime, takeMoney = True }
        , { space = "347", sound = Door, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "367", sound = Door, takeMoney = False }
        , { space = "376", sound = Crime, takeMoney = True }
        , { space = "386", sound = Door, takeMoney = False }
        , { space = "797", sound = Street, takeMoney = False }
        , { space = "500", sound = Subway, takeMoney = False }
        , { space = "501", sound = Street, takeMoney = False }
        , { space = "113", sound = Door, takeMoney = False }
        , { space = "123", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
