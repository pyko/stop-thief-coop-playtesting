module Paths.Path6 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 6
    , difficulty = Easy
    , startBuilding = SWINNERTONS
    , futureMoves =
        [ { space = "272", sound = Footsteps, takeMoney = False }
        , { space = "263", sound = Door, takeMoney = False }
        , { space = "252", sound = Footsteps, takeMoney = False }
        , { space = "242", sound = Crime, takeMoney = True }
        , { space = "233", sound = Footsteps, takeMoney = False }
        , { space = "223", sound = Footsteps, takeMoney = False }
        , { space = "214", sound = Door, takeMoney = False }
        , { space = "703", sound = Street, takeMoney = False }
        , { space = "701", sound = Street, takeMoney = False }
        , { space = "699", sound = Subway, takeMoney = False }
        , { space = "679", sound = Street, takeMoney = False }
        , { space = "278", sound = Door, takeMoney = False }
        , { space = "277", sound = Crime, takeMoney = True }
        , { space = "276", sound = Footsteps, takeMoney = False }
        , { space = "265", sound = Crime, takeMoney = True }
        , { space = "255", sound = Door, takeMoney = False }
        , { space = "246", sound = Crime, takeMoney = True }
        , { space = "235", sound = Window, takeMoney = False }
        , { space = "224", sound = Footsteps, takeMoney = False }
        , { space = "214", sound = Door, takeMoney = False }
        , { space = "703", sound = Street, takeMoney = False }
        , { space = "701", sound = Street, takeMoney = False }
        , { space = "699", sound = Subway, takeMoney = False }
        , { space = "679", sound = Street, takeMoney = False }
        , { space = "278", sound = Door, takeMoney = False }
        , { space = "267", sound = Door, takeMoney = False }
        , { space = "247", sound = Crime, takeMoney = True }
        , { space = "237", sound = Door, takeMoney = False }
        , { space = "228", sound = Window, takeMoney = False }
        , { space = "619", sound = Street, takeMoney = False }
        , { space = "709", sound = Street, takeMoney = False }
        , { space = "719", sound = Street, takeMoney = False }
        , { space = "328", sound = Door, takeMoney = False }
        , { space = "337", sound = Crime, takeMoney = True }
        , { space = "347", sound = Door, takeMoney = False }
        , { space = "357", sound = Footsteps, takeMoney = False }
        , { space = "367", sound = Door, takeMoney = False }
        , { space = "376", sound = Crime, takeMoney = True }
        , { space = "365", sound = Door, takeMoney = False }
        , { space = "335", sound = Footsteps, takeMoney = False }
        , { space = "344", sound = Door, takeMoney = False }
        , { space = "333", sound = Footsteps, takeMoney = False }
        , { space = "332", sound = Door, takeMoney = False }
        , { space = "342", sound = Crime, takeMoney = True }
        , { space = "352", sound = Crime, takeMoney = True }
        , { space = "362", sound = Door, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "371", sound = Window, takeMoney = False }
        , { space = "362", sound = Door, takeMoney = False }
        , { space = "351", sound = Window, takeMoney = False }
        , { space = "352", sound = Footsteps, takeMoney = False }
        , { space = "362", sound = Door, takeMoney = False }
        , { space = "371", sound = Window, takeMoney = False }
        , { space = "471", sound = Door, takeMoney = False }
        , { space = "472", sound = Footsteps, takeMoney = False }
        , { space = "473", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
