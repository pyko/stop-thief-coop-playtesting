module Paths.Path3 exposing (path)

import Model exposing (Building(..), Difficulty(..), Path, Sound(..))


path : Path
path =
    { number = 3
    , difficulty = Medium
    , startBuilding = MUSEUM
    , futureMoves =
        [ { space = "326", sound = Footsteps, takeMoney = False }
        , { space = "337", sound = Crime, takeMoney = True }
        , { space = "347", sound = Door, takeMoney = False }
        , { space = "357", sound = Footsteps, takeMoney = False }
        , { space = "367", sound = Door, takeMoney = False }
        , { space = "376", sound = Crime, takeMoney = True }
        , { space = "375", sound = Footsteps, takeMoney = False }
        , { space = "365", sound = Door, takeMoney = False }
        , { space = "355", sound = Crime, takeMoney = True }
        , { space = "344", sound = Door, takeMoney = False }
        , { space = "333", sound = Footsteps, takeMoney = False }
        , { space = "332", sound = Door, takeMoney = False }
        , { space = "331", sound = Door, takeMoney = False }
        , { space = "810", sound = Street, takeMoney = False }
        , { space = "500", sound = Subway, takeMoney = False }
        , { space = "610", sound = Street, takeMoney = False }
        , { space = "131", sound = Door, takeMoney = False }
        , { space = "122", sound = Footsteps, takeMoney = False }
        , { space = "123", sound = Crime, takeMoney = True }
        , { space = "124", sound = Door, takeMoney = False }
        , { space = "144", sound = Crime, takeMoney = True }
        , { space = "153", sound = Footsteps, takeMoney = False }
        , { space = "154", sound = Footsteps, takeMoney = False }
        , { space = "144", sound = Footsteps, takeMoney = False }
        , { space = "125", sound = Door, takeMoney = False }
        , { space = "136", sound = Door, takeMoney = False }
        , { space = "146", sound = Crime, takeMoney = True }
        , { space = "157", sound = Door, takeMoney = False }
        , { space = "168", sound = Door, takeMoney = False }
        , { space = "579", sound = Street, takeMoney = False }
        , { space = "500", sound = Subway, takeMoney = False }
        , { space = "501", sound = Street, takeMoney = False }
        , { space = "413", sound = Door, takeMoney = False }
        , { space = "423", sound = Footsteps, takeMoney = False }
        , { space = "432", sound = Footsteps, takeMoney = False }
        , { space = "442", sound = Footsteps, takeMoney = False }
        , { space = "463", sound = Footsteps, takeMoney = False }
        , { space = "473", sound = Crime, takeMoney = True }
        , { space = "464", sound = Door, takeMoney = False }
        , { space = "475", sound = Crime, takeMoney = True }
        , { space = "456", sound = Door, takeMoney = False }
        , { space = "445", sound = Crime, takeMoney = True }
        , { space = "444", sound = Door, takeMoney = False }
        , { space = "443", sound = Footsteps, takeMoney = False }
        , { space = "432", sound = Footsteps, takeMoney = False }
        , { space = "423", sound = Footsteps, takeMoney = False }
        , { space = "424", sound = Door, takeMoney = False }
        , { space = "425", sound = Crime, takeMoney = True }
        ]
    , movesTaken = []
    }
