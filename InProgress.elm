module InProgress
    exposing
        ( view
        )

import Clue
import Html exposing (..)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick, onInput)
import MakeArrest
import Model exposing (..)
import PrivateTip
import Styled exposing (..)
import Views.History as History


button =
    styled Html.button
        [ backgroundColor (hex "dbe4ff")
        , padding (px 5)
        , fontSize (px 15)
        , marginBottom (px 10)
        , marginRight (px 10)
        ]


smallText =
    styled div
        [ fontSize (px 12)
        ]


view : InProgressState -> Game -> Html Msg
view playingState game =
    let
        mainView =
            case playingState of
                ShowHint ->
                    Clue.view game.path

                ConfirmPrivateTip move ->
                    PrivateTip.confirm move

                ShowPrivateTip move ->
                    PrivateTip.show move

                ConfirmMakeArrest ->
                    MakeArrest.inputView game

                MakeArrestResult Success ->
                    MakeArrest.successful

                MakeArrestResult Failed ->
                    MakeArrest.failed
    in
    div []
        [ div []
            [ strong [] [ text <| "Cash: $" ++ toString game.cash ]
            , br [] []
            , strong [] [ text <| "Total arrests: " ++ toString game.arrests ++ "/12" ]
            , br [] []
            , strong [] [ text <| "Crimes committed: " ++ toString game.crimesCommitted ]
            ]
        , div [ style [ ( "height", "300px" ) ] ]
            [ mainView ]
        , hr [] []
        , History.view game.path
        ]
