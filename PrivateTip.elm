module PrivateTip exposing (confirm, displayPrivateTip, show)

import Html exposing (..)
import Html.Events exposing (onClick)
import Model exposing (..)
import Styled exposing (..)


-- UPDATE


displayPrivateTip : Game -> Move -> Game
displayPrivateTip game move =
    { game
        | state = InProgress <| ShowPrivateTip move
        , cash = game.cash - 1000
    }



-- VIEW


button =
    styled Html.button
        [ backgroundColor (hex "dbe4ff")
        , padding (px 5)
        , fontSize (px 15)
        , marginBottom (px 10)
        , marginRight (px 10)
        ]


show : Move -> Html Msg
show move =
    div []
        [ h1 [] [ text <| String.left 1 move.space ++ "??" ]
        , button [ onClick CancelAction ] [ text "Ok" ]
        ]


confirm : Move -> Html Msg
confirm move =
    div []
        [ h3 [] [ text "Spend $1000 to get private tip?" ]
        , button [ onClick <| DisplayPrivateTip move ] [ text "Yes!" ]
        , button [ onClick CancelAction ] [ text "Cancel" ]
        ]
