module MakeArrest
    exposing
        ( arrestResult
        , failed
        , inputView
        , result
        , successful
        )

import Html exposing (..)
import Html.Attributes exposing (disabled, maxlength, placeholder, type_)
import Html.Events exposing (onClick, onInput)
import Model exposing (..)
import Styled exposing (..)


-- UPDATE


result : Game -> ArrestResult -> Game
result game state =
    case state of
        Success ->
            { game
                | state = InProgress <| MakeArrestResult Success
                , arrests = game.arrests + 1
            }

        Failed ->
            { game
                | state = InProgress <| MakeArrestResult Failed
                , cash = game.cash - 1000
                , arrestSpaceInput = Nothing
            }


arrestResult : String -> String -> ArrestResult
arrestResult userGuess actual =
    if userGuess == actual then
        Success
    else
        Failed



-- VIEW


button =
    styled Html.button
        [ backgroundColor (hex "dbe4ff")
        , padding (px 5)
        , fontSize (px 15)
        , marginBottom (px 10)
        , marginRight (px 10)
        ]


arrestInput =
    styled input
        [ fontSize (px 20)
        , fontFamilies [ "Monaco" ] monospace
        , display block
        , margin auto
        , marginTop (px 20)
        , marginBottom (px 20)
        , padding (px 5)
        , width (Styled.em 2.5)
        , textAlign center
        ]


inputView : Game -> Html Msg
inputView game =
    let
        arrestButtonAttr =
            case ( game.arrestSpaceInput, List.head game.path.movesTaken ) of
                ( Just guess, Just move ) ->
                    if String.length guess == 3 then
                        onClick <| DisplayArrestResult <| arrestResult guess move.space
                    else
                        disabled True

                _ ->
                    disabled True
    in
    div []
        [ arrestInput
            [ type_ "tel"
            , placeholder "???"
            , maxlength 3
            , onInput ArrestSpaceInput
            ]
            []
        , button [ arrestButtonAttr ] [ text "Make Arrest" ]
        , button [ onClick CancelAction ] [ text "Cancel" ]
        ]


successful : Html Msg
successful =
    div []
        [ h3 [] [ text "Yay, you caught a thief!" ]
        , p [] [ text "Note, thieves will now make 2 silent moves..." ]
        , button [ onClick TakeTwoSilentMoves ] [ text "Ok" ]
        ]


failed : Html Msg
failed =
    div []
        [ h3 [] [ text "Wrong! Lost $1000" ]
        , button [ onClick CancelAction ] [ text "Ok" ]
        ]
