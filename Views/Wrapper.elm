module Views.Wrapper exposing (..)

import Html exposing (..)
import Model exposing (..)
import Styled exposing (..)


wrapper =
    styled div
        [ padding (px 10)
        , fontFamilies [ "Monaco" ] monospace
        , textAlign center
        ]


view : Html Msg -> Html Msg
view currentView =
    wrapper []
        [ h1 [] [ text "Stop Thief" ]
        , currentView
        ]
