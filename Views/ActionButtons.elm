module Views.ActionButtons exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Model exposing (..)
import Styled exposing (..)


-- VIEW


button =
    styled Html.button
        [ backgroundColor (hex "dbe4ff")
        , padding (px 5)
        , fontSize (px 15)
        , marginBottom (px 10)
        , marginRight (px 10)
        ]


largeButton =
    styled button
        [ backgroundColor (hex "dbe4ff")
        , padding (px 10)
        , Styled.width (px 200)
        , fontSize (px 15)
        , marginBottom (px 20)
        ]


mainButton =
    styled largeButton
        [ backgroundColor (hex "1b6ec2")
        , fontWeight (int 600)
        , fontSize (px 20)
        , color (hex "fff")
        ]


view : Path -> Html Msg
view path =
    div []
        [ div [] [ nextClue ]
        , div [] [ getPrivateTip path ]
        , div [] [ makeArrest ]
        ]


nextClue : Html Msg
nextClue =
    mainButton [ onClick NextClue ] [ text "Next Clue" ]


getPrivateTip : Path -> Html Msg
getPrivateTip path =
    let
        privateTipButton =
            case List.head path.movesTaken of
                Just move ->
                    onClick <| ConfirmGetPrivateTip move

                Nothing ->
                    disabled True
    in
    largeButton [ privateTipButton ] [ text "Get Private Tip" ]


makeArrest : Html Msg
makeArrest =
    largeButton [ onClick MakeArrestInput ] [ text "Make Arrest" ]
