module Views.History exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Model exposing (..)
import Styled exposing (..)


ol =
    styled Html.ol
        [ display inlineBlock
        , textAlign left_
        ]


view : Path -> Html Msg
view path =
    ol [ reversed True ]
        (List.map moveTakenView path.movesTaken)


moveTakenView : Move -> Html Msg
moveTakenView move =
    li [] [ text <| toString move.sound ]
