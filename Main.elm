module Main exposing (..)

import Clue
import Html exposing (..)
import Html.Attributes exposing (..)
import InProgress exposing (..)
import MakeArrest
import Model exposing (..)
import PreGame
import PrivateTip
import Views.Wrapper as Wrapper


-- UPDATE


update : Msg -> Game -> Game
update msg game =
    endGameIfNeeded <| handleMsg msg game


handleMsg : Msg -> Game -> Game
handleMsg msg game =
    case msg of
        StartGame path ->
            startGame path game

        NextClue ->
            Clue.nextClue game

        ArrestSpaceInput space ->
            { game | arrestSpaceInput = Just space }

        MakeArrestInput ->
            { game | state = InProgress ConfirmMakeArrest }

        DisplayArrestResult arrestResult ->
            MakeArrest.result game arrestResult

        TakeTwoSilentMoves ->
            Clue.takeTwoSilentMoves game

        ConfirmGetPrivateTip move ->
            { game | state = InProgress <| ConfirmPrivateTip move }

        DisplayPrivateTip move ->
            PrivateTip.displayPrivateTip game move

        CancelAction ->
            { game | state = InProgress ShowHint }


startGame : Path -> Game -> Game
startGame path game =
    Clue.nextClue
        { game
            | state = InProgress ShowHint
            , path = path
        }


endGameIfNeeded : Game -> Game
endGameIfNeeded game =
    if game.cash <= 0 || game.arrests == 12 then
        { game | state = PostGame }
    else
        game



-- VIEW


view : Game -> Html Msg
view game =
    let
        gameView =
            case game.state of
                PreGame ->
                    PreGame.view

                InProgress playingState ->
                    InProgress.view playingState game

                PostGame ->
                    endGame game
    in
    Wrapper.view <| gameView


endGame : Game -> Html Msg
endGame game =
    let
        ( heading, message ) =
            if game.arrests == 12 then
                ( "Time to retire :)"
                , "Good job, you caught the Atlantic 12!"
                )
            else
                ( "Better luck next time :("
                , "Those nasty thieves are still out there!"
                )
    in
    div []
        [ h1 [] [ text heading ]
        , p [] [ text message ]
        , a
            [ href "https://goo.gl/forms/clzk63WMtlqLE8s53"
            , target "_blank"
            , style
                [ ( "fontWeight", "bold" )
                , ( "fontSize", "x-large" )
                ]
            ]
            [ text "Fill in playtest form" ]
        , p
            [ style
                [ ( "fontWeight", "bold" )
                , ( "fontSize", "larger" )
                ]
            ]
            [ text <| "Path played: " ++ toString game.path.number ]
        , p
            [ style
                [ ( "fontWeight", "bold" )
                , ( "fontSize", "larger" )
                ]
            ]
            [ text <| "Arrests made: " ++ toString game.arrests ]
        , p [] [ text <| "Money left: " ++ formatCash game.cash ]
        , p [] [ text <| "Crimes committed: " ++ toString game.crimesCommitted ]
        ]


formatCash : Int -> String
formatCash cash =
    if cash < 0 then
        "-$" ++ toString (Basics.abs cash)
    else
        "$" ++ toString cash



-- MAIN


main : Program Never Game Msg
main =
    Html.beginnerProgram
        { model = PreGame.initGame
        , view = view
        , update = update
        }
