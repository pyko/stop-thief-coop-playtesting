module PreGame exposing (initGame, view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Model exposing (..)
import Paths.Path1 as Path1
import Paths.Path2 as Path2
import Paths.Path3 as Path3
import Paths.Path4 as Path4
import Paths.Path5 as Path5
import Paths.Path6 as Path6
import Paths.Path7 as Path7
import Paths.Path8 as Path8
import Styled exposing (..)


initGame : Game
initGame =
    { path = Path1.path
    , cash = 50000
    , arrests = 0
    , state = PreGame
    , arrestSpaceInput = Nothing
    , crimesCommitted = 0
    }


button =
    styled Html.button
        [ backgroundColor (hex "dbe4ff")
        , padding (px 15)
        , fontSize (px 20)
        , marginBottom (px 10)
        , marginRight (px 10)
        ]


view : Html Msg
view =
    div []
        [ startGameWithPath Path1.path
        , startGameWithPath Path2.path
        , p [] []
        , startGameWithPath Path3.path
        , startGameWithPath Path4.path
        , p [] []
        , startGameWithPath Path5.path
        , startGameWithPath Path6.path
        , p [] []
        , startGameWithPath Path7.path
        , startGameWithPath Path8.path
        ]


startGameWithPath : Path -> Html Msg
startGameWithPath path =
    button
        [ onClick <| StartGame path
        , title <| toString path.difficulty
        ]
        [ text <| pathName path ]


pathName : Path -> String
pathName path =
    "Path " ++ toString path.number
