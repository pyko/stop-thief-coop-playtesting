module Model exposing (..)


type Msg
    = StartGame Path
    | NextClue
    | MakeArrestInput
    | DisplayArrestResult ArrestResult
    | ArrestSpaceInput String
    | ConfirmGetPrivateTip Move
    | DisplayPrivateTip Move
    | CancelAction
    | TakeTwoSilentMoves


type alias Game =
    { path : Path
    , cash : Int
    , arrests : Int
    , state : GameState
    , arrestSpaceInput : Maybe String
    , crimesCommitted : Int
    }


type GameState
    = PreGame
    | InProgress InProgressState
    | PostGame


type InProgressState
    = ShowHint
    | ConfirmPrivateTip Move
    | ShowPrivateTip Move
    | ConfirmMakeArrest
    | MakeArrestResult ArrestResult


type ArrestResult
    = Success
    | Failed


type alias Path =
    { number : Int
    , difficulty : Difficulty
    , startBuilding : Building
    , futureMoves : List Move
    , movesTaken : List Move
    }


type Difficulty
    = Easy
    | Medium
    | Hard


type alias Move =
    { space : String
    , sound : Sound
    , takeMoney : Bool
    }


type Sound
    = Door
    | Crime
    | Window
    | Footsteps
    | Street
    | Subway
    | Hold
    | Silent


type Building
    = DRDOYLE
    | SWINNERTONS
    | MUSEUM
    | BANK
